FROM node
COPY . .
RUN npm install
RUN npm run build

FROM nginx:alpine
COPY --from=0 dist /usr/share/nginx/html/dist
COPY index.html /usr/share/nginx/html
COPY media /usr/share/nginx/html/media
