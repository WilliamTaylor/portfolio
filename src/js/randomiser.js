function random(min, max) {
    return Math.floor(Math.random()*(max-min+1)+min);
}

export default {
    start() {
        const logoObject = document.getElementById('logoObject');
        const logo = logoObject.contentDocument;
        const lightPaths = logo.getElementsByClassName('light');
        const darkPaths = logo.getElementsByClassName('dark');

        const lightColours = ['#5fa1df', '#458dba', '#70beee', '#72b1e5',
            '#2e8abd', '#84afcd', '#95c4ee', '#cbe6f9', '#8ac6f0',
            '#9dbedb', '#a6cef0', '#719fd2', '#6fa3d5', '#88bbde',
            '#3e92cc', '#87b2e6', '#70beee', '#b4dbf6',
            '#5ba9ee', '#5fade3'];

        const darkColours = ['#083c69', '#0a2f63', '#081848', '#143765',
            '#0d3065', '#172d65', '#1f559c', '#046795', '#052b84',
            '#043063', '#00439c', '#053357', '#111a4e', '#0e2954'];

        const lastLightColourIndex = lightColours.length - 1;
        const lastLightPathIndex = lightPaths.length - 1;
        const lastDarkColourIndex = darkColours.length - 1;
        const lastDarkPathIndex = darkPaths.length - 1;

        let isLightTurn = true;

        setInterval(function () {

            if (isLightTurn) {
                lightPaths[random(0, lastLightPathIndex)].style.fill = lightColours[random(0, lastLightColourIndex)];
            }

            else darkPaths[random(0, lastDarkPathIndex)].style.fill = darkColours[random(0, lastDarkColourIndex)];

            isLightTurn = !isLightTurn;

        }, 100);
    }
}