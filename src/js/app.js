import '../scss/portfolio.scss';
import Randomiser from './randomiser';

window.onload = () => {
    Randomiser.start();
};